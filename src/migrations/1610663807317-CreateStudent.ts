import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateStudent1610663807317 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.createTable(new Table({
      name: 'student',
      columns: [
        {
          name: 'idstudent',
          type: 'serial',
          isPrimary: true,
          generationStrategy: 'increment'
        },
        {
          name: 'name',
          type: 'varchar',
          isNullable: false
        },
        {
          name: 'key',
          type: 'varchar',
          isUnique: true
        },
        {
          name: 'create_at',
          type: 'timestamp',
          default: 'now()'
        },
        {
          name: 'update_at',
          type: 'timestamp',
          default: 'now()'
        }
      ]
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropTable('student')
  }

}
