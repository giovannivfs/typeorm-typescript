import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateContent1610663799225 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.createTable(new Table({
      name: 'content',
      columns: [
        {
          name: 'idcontent',
          type: 'serial',
          isPrimary: true,
          generationStrategy: 'increment'
        },
        {
          name: 'description',
          type: 'varchar',
          isNullable: false
        },
        {
          name: 'linkcontent',
          type: 'varchar'
        },
        {
          name: 'create_at',
          type: 'timestamp',
          default: 'now()'
        },
        {
          name: 'update_at',
          type: 'timestamp',
          default: 'now()'
        }
      ]
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropTable('content')
  }

}
