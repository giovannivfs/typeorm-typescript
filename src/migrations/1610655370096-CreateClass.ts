import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateClass1610655370096 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(new Table({
        name: 'class',
        columns: [
          {
            name: 'idclass',
            type: 'serial',
            isPrimary: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar'
          },
          {
            name: 'duration',
            type: 'integer'
          },
          {
            name: 'create_at',
            type: 'timestamp',
            default: 'now()'
          },
          {
            name: 'update_at',
            type: 'timestamp',
            default: 'now()'
          }
        ]
      })
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropTable('class')
  }

}
