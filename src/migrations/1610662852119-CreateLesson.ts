import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateLesson1610662852119 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.createTable(new Table({
      name: 'lesson',
      columns: [
        {
          name: 'idlesson',
          type: 'serial',
          isPrimary: true,
          generationStrategy: 'increment'
        },
        {
          name: 'description',
          type: 'varchar',
          isUnique: true,
          isNullable: false
        },
        {
          name: 'create_at',
          type: 'timestamp',
          default: 'now()'
        },
        {
          name: 'update_at',
          type: 'timestamp',
          default: 'now()'
        }
      ]
    }))
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropTable('lesson')
  }

}
