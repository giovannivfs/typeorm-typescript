import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";

@Entity()
export default class Student{
  @PrimaryGeneratedColumn('increment')
  idstudent: number

  @Column()
  name: string

  @Column({unique: true})
  key: string

  @CreateDateColumn()
  create_at: Date

  @UpdateDateColumn()
  update_at: Date
}