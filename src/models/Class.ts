import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";

@Entity()
export default class Class{
  @PrimaryGeneratedColumn('increment')
  idclass: number

  @Column({unique: true})
  name: string

  @Column()
  duration: number

  @CreateDateColumn()
  create_at: Date

  @UpdateDateColumn()
  update_at: Date
}