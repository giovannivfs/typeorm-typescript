import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn} from "typeorm";

@Entity()
export default class Content{
  @PrimaryGeneratedColumn('increment')
  idcontent: number

  @Column()
  description: string

  @Column()
  linkcontent: string

  @CreateDateColumn()
  create_at: Date

  @UpdateDateColumn()
  update_at: Date
}